import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductService } from './product.service';
import { proveedor, productos } from './products.model';

@Component({
  selector: 'page-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  @Input() nombre: string;
  products: Observable<productos[]>;
  constructor(private productServices: ProductService) { }

  ngOnInit() {

  }
  ionViewWillEnter(){
    console.log(this.productServices.fetchProduct().subscribe());
    this.products = this.productServices.getAll();
    console.log(this.products);
    
  }
}
