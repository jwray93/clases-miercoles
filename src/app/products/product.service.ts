import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { productos, proveedor } from './products.model';
import {tap, map} from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private proveedore: proveedor[] = [
    {
        nombre: "Intcomex",
        cedula: 11111,
        direccion: "San Jose",
        numero: 8888,
        correo: "www@deee.com",
        codigo: 333
    },
    {
        nombre: "Intcomex",
        cedula: 11111,
        direccion: "San Jose",
        numero: 8888,
        correo: "www@deee.com",
        codigo: 333
    }
  ];
  private productos = new BehaviorSubject<productos[]>([]);
  constructor(
    private http: HttpClient,
    
  ) { }
  getAll(){
    console.log(this.productos);
    return this.productos.asObservable();
  }
  getProduct(productId: number){
    /*return {
      ...this.productos.find(
        product => {
          return product.codigo === productId;
        }
      )
    };*/
  }
  fetchProduct(){
    return this.http
      .get<{[key: string]:  productos}>('https://clasesmiercoles-49dde.firebaseio.com/addProduct.json')
      .pipe(map(restData => {
        const products = [];
        for(const key in restData){
          console.log(key);
          if(restData.hasOwnProperty(key)){
            const current_product: productos = {
              proveedor: this.proveedore,
              precio: 3,
              candidad: restData[key].candidad,
              codigo: restData[key].codigo,
              nombre: restData[key].nombre,
              peso: restData[key].peso,
              fecha_caducidad: restData[key].fecha_caducidad,
              descripcion: restData[key].descripcion
            }
            console.log(current_product);
            products.push(current_product);
          }
        }
        return products;
      }), tap( products => {
        this.productos.next(products);
      }));
  }
  deleteProduct(productId: number){
    /*this.productos = this.productos.filter(
      product => {
        return product.codigo !== productId;
      }
    );*/
  }
  addProduct(pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pfecha_caducidad: string,
    pdescripcion: string){

      const product: productos = {
        proveedor: this.proveedore,
        precio: pprecio,
        candidad: pcantidad,
        codigo: pcodigo,
        nombre: pnombre,
        peso: ppeso,
        fecha_caducidad: pfecha_caducidad,
        descripcion: pdescripcion
      }
      this.http.post('https://clasesmiercoles-49dde.firebaseio.com/addProduct.json',
        { 
          ...product, 
          id:null
        }).subscribe(() => {
          console.log('entro');
        });
      this.fetchProduct;
      
  }
}
